import pyodbc as odbc
import pandas as pd
import numpy as np

# Abre conexao com Dremio
host = 'localhost'
port = 31010
uid = 'luiz.covo'
pwd = '8ud@Dremio'
driver = '/Library/Dremio/ODBC/lib/libdrillodbc_sbu.dylib'

cnxn = odbc.connect("Driver={};ConnectionType=Direct;HOST={};PORT={};AuthenticationType=Plain;UID={};PWD={}".format(driver, host,port,uid,pwd),autocommit=True)

# Classe para geracao de matriz e vetor para treinamento de algoritimos
class DataTrainTest(object):
    def GeraTrainTest(self, sqlTrain, sqlTest):
        dfTrain = pd.read_sql(sqlTrain,cnxn)
        dfTest = pd.read_sql(sqlTest,cnxn)
        dfTrain['Tipo'] = 'Train'
        dfTest['Tipo'] = 'Test'
        dfTest['Survived'] = np.nan
        dfMerged = pd.concat([dfTrain, dfTest], axis=0, sort=True)
        dfMerged['FareBin'] = pd.qcut(dfMerged.Fare, 6)
        dfMerged['AgeBin'] = pd.qcut(dfMerged.Age, 6)
        targetVar = ['Survived']
        VarDrop = ['PassengerId', 'Fare', 'Tipo', 'Age']
        TotalVarDrop = targetVar + VarDrop
        dfFiltered = dfMerged.drop(TotalVarDrop, axis=1)
        dfDummies = pd.get_dummies(dfFiltered)
        self.XTrain = dfDummies.iloc[:len(dfTrain),:].values
        self.XTest = dfDummies.iloc[len(dfTrain):,:].values
        self.YTrain = dfMerged['Survived'].iloc[:len(dfTrain)].values

# Geracao de Dados Titanic
#####################################################
sqlTrain = '''SELECT * FROM MyLab.TrainTitanic'''
sqlTest = '''SELECT * FROM MyLab.TestTitanic'''

DataTitanic = DataTrainTest()
DataTitanic.GeraTrainTest(sqlTrain, sqlTest)